/*
 *   This file is part of Auralquiz
 *   Copyright 2011-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef AURALWINDOW_H
#define AURALWINDOW_H

#include <QWidget>
#include <QSettings>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QPixmap>
#include <QMovie>
#include <QPushButton>
#include <QProgressBar>
#include <QLCDNumber>
#include <QCloseEvent>
#include <QTimer>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QGroupBox>
#include <QMessageBox>
#include <QTime>
#include <QKeySequence>
#include <QLibraryInfo>
#include <QStandardPaths>   // Qt 6 needs the explicit include
#include <QRandomGenerator>

#include <QDebug>

#if QT_VERSION_MAJOR == 6
  #include <phonon4qt6/phonon/MediaObject>
  #include <phonon4qt6/phonon/AudioOutput>
  #include <phonon4qt6/phonon/BackendCapabilities>
  #include <phonon4qt6/phonon/Global>
#else
  #include <phonon4qt5/phonon/MediaObject>
  #include <phonon4qt5/phonon/AudioOutput>
  #include <phonon4qt5/phonon/BackendCapabilities>
  #include <phonon4qt5/phonon/Global>
#endif


#include "musicanalyzer.h"
#include "optionsdialog.h"
#include "answerbox.h"
#include "ranking.h"



#define MAXPLAYERS 8


class AuralWindow : public QWidget
{
    Q_OBJECT

public:
    AuralWindow(QWidget *parent = 0);
    ~AuralWindow();

    void shuffleMusicFiles();
    void updateStatistics();

    void initWelcomeScreen();
    void initPlayingScreen();

    void toggleScreen();

    void setThemedColors();

    void enablePlayerInput(bool state);


protected:
    void closeEvent(QCloseEvent *event);


public slots:
    void playerStateChanged(Phonon::State newstate,
                            Phonon::State oldstate);

    void timerTick();

    void updateConfig(bool startGame, QStringList directories,
                      bool forceReload, int difficulty,
                      int questions, int players,
                      QStringList playerNameList, bool ownColors);
    void showAbout();

    void modifyStartGameButton(bool enabledState,
                               QString text,
                               QString tooltip);


    void newQuestion();
    void nextSongLoaded();
    void preQuestion();

    void answerQuestion(int numAnswer);
    void answer1();
    void answer2();
    void answer3();
    void answer4();
    void answerFromAnswerBox(bool correct);

    void confirmEndGame();

    void killRanking();

private:
    QVBoxLayout *m_mainLayout;

    QWidget *m_welcomeWidget;
    QVBoxLayout *m_welcomeLayout;
    QLabel *m_logoLabel;
    QPushButton *m_startGameButton;
    QPushButton *m_configureButton;
    QPushButton *m_aboutButton;
    QPushButton *m_quitButton;
    QAction *m_quitAction;
    QTimer *m_postInitTimer;
    QTimer *m_postConfigUpdatedTimer;


    QWidget *m_playingWidget;
    QVBoxLayout *m_playingLayout;
    QVBoxLayout *m_playingTopLayout;
    QHBoxLayout *m_playingMiddleLayout;
    QVBoxLayout *m_statisticsLayout;
    QVBoxLayout *m_answersLayout;
    QHBoxLayout *m_playingProgressLayout;
    QHBoxLayout *m_playingBottomLayout;

    QTimer *preQuestionTimer;
    QTimer *postQuestionTimer;

    QLabel *playerNameLabel;
    QLabel *questionLabel;

    QLabel *infoLabel;
    //const QString infoLabelCorrectStyle;
    //const QString infoLabelWrongStyle;
    //const QString infoLabelTimeUpStyle;

    QLabel *aniNoteLabel;
    QMovie aniNoteMovie;
    QGroupBox *m_statisticsBox;
    QVBoxLayout *statisticsBoxLayout;
    QLabel *m_statisticsLabel;
    QTimer *gameTimer;
    QProgressBar *m_timeBar;
    QProgressBar *gameProgressBar;
    QLabel *gameScoreLabel;
    QLCDNumber *gameScore;

    QLabel *m_statusLabel;
    QPushButton *m_endGameButton;

    QPushButton *answerButton[4];
    QAction *answerAction1;
    QAction *answerAction2;
    QAction *answerAction3;
    QAction *answerAction4;

    Phonon::AudioOutput *musicOutput;
    Phonon::MediaObject *musicPlayer;

    OptionsDialog *optionsDialog; // Options (configuration) window

    MusicAnalyzer *musicAnalyzer;


    bool m_firstRun;
    bool useOwnColorTheme;

    bool playing;

    int difficultyLevel;
    int numQuestions;
    int numPlayers;
    int m_currentPlayer;

    QStringList playerNames;

    int m_maxTime; // Depends on difficulty
    int warningTime;
    int dangerTime;

    int pieceDuration;

    int m_questionType;
    int correctAnswer;

    int currentMusicFile;
    QStringList m_musicDirectories;
    QStringList musicFiles[4];
    QString dataDirectory;


    // For statistics
    QList<int> goodAnswers;
    QList<int> badAnswers;
    QList<int> timedOutAnswers;
    QList<int> score;

    // Widget for the "type-the-answer" mode
    AnswerBox *m_answerBox;

    // Ranking window
    Ranking *m_rankingWindow;
    QTimer *m_rankingTimer;
};



#endif // AURALWINDOW_H
