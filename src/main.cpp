/*
 *   This file is part of Auralquiz
 *   Copyright 2011-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include <QApplication>
#include <QTranslator>
#include <QtDebug>
#include <iostream>

#include "auralwindow.h"


void customMessageHandler(QtMsgType type,
                          const QMessageLogContext &context,
                          const QString &msg)
{
    Q_UNUSED(type)
    Q_UNUSED(context)
    Q_UNUSED(msg)

    // Do nothing

    return;
}


int main(int argc, char *argv[])
{
    QApplication auralApp(argc, argv);

    auralApp.setApplicationName("Auralquiz");
    auralApp.setApplicationVersion("1.1.0-dev+9");
    auralApp.setOrganizationName("JanCoding");
    auralApp.setOrganizationDomain("jancoding.wordpress.com");
    // This is mainly needed for window managers under Wayland
    auralApp.setDesktopFileName(QStringLiteral("org.nongnu.auralquiz.desktop"));

    std::cout << QString("Auralquiz v%1 - JanKusanagi 2011-2024\n"
                         "https://jancoding.wordpress.com/auralquiz\n\n")
                 .arg(auralApp.applicationVersion()).toStdString();

    std::cout << QString("- Built with Qt v%1, Phonon v%2\n")
                 .arg(QT_VERSION_STR).arg(PHONON_VERSION_STR).toStdString();
    std::cout << QString("- Running with Qt v%1, Phonon v%2\n\n")
                 .arg(qVersion()).arg(Phonon::phononVersion()).toStdString();
    std::cout.flush();


    // Register custom message handler, to hide debug messages unless specified
    if (qApp->arguments().contains("--debug", Qt::CaseInsensitive))
    {
        std::cout << "Debug messages enabled\n";
    }
    else
    {
        std::cout << "To see debug messages while running, use --debug\n";
        qInstallMessageHandler(customMessageHandler);
    }


    // Enable localization

    // Get language from LANG environment variable or system's locale
    QString languageString = qgetenv("LANG");
    if (languageString.isEmpty())
    {
        languageString = QLocale::system().name();
    }

    QString languageFile;
    bool languageLoaded;


    QTranslator translatorQt;
    languageFile = QString("qt_%1").arg(languageString);
    std::cout << "\n"
              << "Using Qt translation "
              << QLibraryInfo::location(QLibraryInfo::TranslationsPath).toStdString()
              << "/" << languageFile.toStdString() << "... ";
    languageLoaded = translatorQt.load(languageFile,
                                       QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    if (languageLoaded)
    {
        std::cout << "OK";
        auralApp.installTranslator(&translatorQt);
    }
    else
    {
        std::cout << "Unavailable";
    }


    QTranslator translatorAuralquiz;
    languageFile = QString(":/translations/auralquiz_%1").arg(languageString);
    std::cout << "\n"
              << "Using program translation "
              << languageFile.toStdString() << "... ";

    languageLoaded = translatorAuralquiz.load(languageFile);
    if (languageLoaded)
    {
        std::cout << "OK";
        auralApp.installTranslator(&translatorAuralquiz);
    }
    else
    {
        std::cout << "Unavailable";
    }


    std::cout << "\n\n";
    std::cout.flush();



    AuralWindow auralWindow;
    auralWindow.show();

    return auralApp.exec();
}
