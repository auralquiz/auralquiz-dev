<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>AnswerBox</name>
    <message>
        <location filename="../src/answerbox.cpp" line="29"/>
        <source>Type the answer here</source>
        <translation>Tecleja la resposta aqui</translation>
    </message>
</context>
<context>
    <name>AuralWindow</name>
    <message>
        <location filename="../src/auralwindow.cpp" line="312"/>
        <source>&amp;Start game</source>
        <translation>Començar &amp;joc</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="328"/>
        <source>&amp;About...</source>
        <translation>So&amp;bre...</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>&amp;Sortir</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="430"/>
        <source>Remaining time to answer this question</source>
        <translation>Temps restant per respondre la pregunta</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="434"/>
        <source>%v out of %m questions - %p%</source>
        <translation>%v de %m preguntes - %p%</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="437"/>
        <source>How many questions you&apos;ve answered</source>
        <translation>Quantes preguntes has contestat</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="439"/>
        <source>Score</source>
        <translation>Puntuació</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="446"/>
        <source>Your current score</source>
        <translation>La teva puntuació actual</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="454"/>
        <source>&amp;End game</source>
        <translation>&amp;Finalitzar joc</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="482"/>
        <source>Statistics</source>
        <translation>Estadístiques</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="664"/>
        <source>Sound system error</source>
        <translation>Error del sistema de so</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="665"/>
        <source>There seems to be a problem with your sound system.</source>
        <translation>Sembla que hi ha un problema amb el teu sistema de so.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="668"/>
        <source>Maybe you don&apos;t have any Phonon backends installed.</source>
        <translation>Potser no tens cap motor de Phonon instal·lat.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="790"/>
        <source>%1 songs available</source>
        <translation>%1 cançons disponibles</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1122"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. Feel free to add your contact information, such as e-mail. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traducció al català per JanKusanagi.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="43"/>
        <source>This seems to be the first time you use Auralquiz.
Before playing, your music will be analyzed.
If needed, you should click the Options button and select the folder where your Ogg, FLAC and MP3 files are stored.

This folder, and sub-folders will be scanned so Auralquiz can generate questions and answers about your music.

You need files correctly tagged in order for the game to work correctly.

The scan can take some time, and the program will not be responsive until it is complete. Please be patient.</source>
        <translation>Sembla que aquesta és la primera vegada que utilitzes Auralquiz.
Abans de jugar, s&apos;analitzarà la teva música.
Si fos necessari, hauríes de fer click al botó d&apos;Opcions i sel·leccionar la carpeta on es troben els teus arxius Ogg, Flac i MP3.

Aquesta carpeta, i sub-carpetes seran analitzades perque Auralquiz pugui generar preguntes i respostes sobre la teva música.

Necessites arxius etiquetats correctament perque el joc funcioni com cal.

L&apos;anàlisis pot trigar una mica, i el programa no respondrà fins que sigui complet. Si us plau, sigues pacient.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="944"/>
        <source>Error playing sound</source>
        <translation>Error reproduint so</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="945"/>
        <source>An error occurred while playing sound.
The error message was: %1

Maybe your Phonon-backend does not have support for the MP3 file format.</source>
        <translation>S&apos;ha produït un error durant la reproducció de so.
El missatge d&apos;error ha sigut: %1

Potser el teu Phonon-backend no té suport pel format d&apos;arxiu MP3.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1030"/>
        <source>Time&apos;s up!!</source>
        <translation>S&apos;ha acabat el temps!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1106"/>
        <source>About Auralquiz</source>
        <translation>Sobre Auralquiz</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1117"/>
        <source>Auralquiz loads all your music from a specified folder and asks questions about it, playing a short piece of each music file as clue.</source>
        <translation>Auralquiz carrega tota la teva música d&apos;una carpeta especificada, i fa preguntes sobre ella, reproduint una petita peça de cada arxiu de música com a pista.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="777"/>
        <source>Starting!</source>
        <translation>Comencem!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="264"/>
        <source>Good</source>
        <translation>Bé</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="273"/>
        <source>Bad</source>
        <translation>Malament</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="282"/>
        <source>Timed out</source>
        <translation>Temps esgotat</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="778"/>
        <source>Let&apos;s go!</source>
        <translation>Anem!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="779"/>
        <source>GO!!</source>
        <translation>Vinga!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="780"/>
        <source>Good luck!</source>
        <translation>Bona sort!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1031"/>
        <source>The answer was:</source>
        <translation>La resposta era:</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1129"/>
        <source>Thanks to all the testers, translators and packagers, who help make Auralquiz better!</source>
        <translation>Gràcies a tots els &apos;testers&apos;, traductors i empaquetadors, que ajuden a fer Auralquiz millor!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1133"/>
        <source>Auralquiz is licensed under the GNU GPL license.</source>
        <translation>Auralquiz es troba llicenciat sota la llicència GNU GPL.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1135"/>
        <source>Main screen image and some icons are based on Oxygen icons, under LGPL license.</source>
        <translation>La imatge de la pantalla principal, i algunes icones, estan basades en Oxygen Icons, sota llicencia LGPL.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1208"/>
        <source>Player %1 of %2</source>
        <translation>Jugador %1 de %2</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1373"/>
        <source>Go, %1!</source>
        <comment>%1=player&apos;s name</comment>
        <translation>Endavant, %1!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1470"/>
        <source>%1 by %2</source>
        <comment>1=song title, 2=author name</comment>
        <translation>%1 de %2</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1475"/>
        <source>+%1 points!</source>
        <translation>+%1 punts!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1501"/>
        <source>Correct answer was:</source>
        <translation>La resposta correcta era:</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1572"/>
        <source>End game?</source>
        <translation>Finalitzar el joc?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1573"/>
        <source>Are you sure you want to end this game?</source>
        <translation>Segur que vols acabar la partida?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;Yes, end it</source>
        <translation>&amp;Si, acabar-la</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="42"/>
        <source>First usage</source>
        <translation>Primer ús</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1319"/>
        <source>Not enough music files</source>
        <translation>No hi ha prou arxius de música</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1320"/>
        <source>You don&apos;t have enough music files, or from enough different artists.
You need music from at least 5 different artists to be able to generate questions.</source>
        <translation>No tens prou arxius de música, o no son de prou artistes diferents.
Necessites música de 5 artistes diferents, com a mínim, pero poder generar preguntes.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1379"/>
        <source>Next!</source>
        <translation>Següent!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1454"/>
        <source>Correct!!</source>
        <translation>Correcte!!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1455"/>
        <source>Yeah!</source>
        <translation>Si!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1456"/>
        <source>That&apos;s right!</source>
        <translation>Així és!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1457"/>
        <source>Good!</source>
        <translation>Bé!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1458"/>
        <source>Great!</source>
        <translation>Genial!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1486"/>
        <source>Wrong!</source>
        <translation>Equivocat!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1487"/>
        <source>No!</source>
        <translation>No!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1489"/>
        <source>Ooops!</source>
        <translation>Ups!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1490"/>
        <source>FAIL!</source>
        <translation>FAIL!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1488"/>
        <source>That&apos;s not it.</source>
        <translation>No, aquesta no.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1408"/>
        <source>All done!</source>
        <translation>Tot fet!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="321"/>
        <source>&amp;Options</source>
        <translation>&amp;Opcions</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="427"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1198"/>
        <source>Who plays this song?</source>
        <translation>Qui toca aquesta cançó?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1202"/>
        <source>What&apos;s the title of this song?</source>
        <translation>Quin es el títol d&apos;aquesta cançó?</translation>
    </message>
</context>
<context>
    <name>MusicAnalyzer</name>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="125"/>
        <source>Configure your music folder</source>
        <translation>Configura la teva carpeta de música</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="214"/>
        <source>Analyzing music. Please wait</source>
        <translation>Analitzant música. Espera, si us plau</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="223"/>
        <source>Getting list of files under
%1</source>
        <comment>%1 is a folder</comment>
        <translation>Llistant arxius a
%1</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="227"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="230"/>
        <source>Listing files...</source>
        <translation>Llistant arxius...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="255"/>
        <location filename="../src/musicanalyzer.cpp" line="361"/>
        <source>Please reload music info</source>
        <translation>Si us plau, recarrega la info de música</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="256"/>
        <source>Music analysis was cancelled</source>
        <translation>S&apos;ha cancel·lat l&apos;anàlisi de la música</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="269"/>
        <source>There are no valid music files in your selected music directory.</source>
        <translation>No hi han arxius de música vàlids al teu directori de música sel·leccionat.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="272"/>
        <source>Please, select another directory containing Ogg, FLAC or MP3 files, and try again.</source>
        <translation>Si us plau, sel·lecciona un altre directori que contingui arxius Ogg, FLAC o MP3, i prova de nou.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="276"/>
        <source>There is no music!</source>
        <translation>No hi ha música!</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="280"/>
        <location filename="../src/musicanalyzer.cpp" line="385"/>
        <source>Please choose another music folder</source>
        <translation>Si us plau, sel·lecciona una altra carpeta de música</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="292"/>
        <source>Analyzing %1 files
under %2</source>
        <comment>%1 is a number, %2 is a folder</comment>
        <translation>Analitzant %1 arxius a %2</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="297"/>
        <source>Cancel analysis</source>
        <translation>Cancel·lar l&apos;anàlisi</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="300"/>
        <source>Analyzing your music...</source>
        <translation>Analitzant la teva música...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="386"/>
        <source>The folder you selected doesn&apos;t have enough valid music files</source>
        <translation>La carpeta que has seleccionat no té prou arxius de música vàlids</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="396"/>
        <source>&amp;Start game</source>
        <translation>Començar &amp;joc</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/optionsdialog.cpp" line="31"/>
        <source>Game options</source>
        <translation>Opcions del joc</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="59"/>
        <source>Using music from:</source>
        <translation>Fent servir música de:</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="64"/>
        <source>Select your &amp;music folder...</source>
        <translation>Sel·lecciona la teva carpeta de &amp;música...</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="83"/>
        <source>&amp;Easier</source>
        <translation>Mes &amp;fàcil</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="98"/>
        <source>&amp;Harder</source>
        <translation>Mes &amp;difícil</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="126"/>
        <source>How many &amp;questions?</source>
        <translation>Quantes &amp;preguntes?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="131"/>
        <source>questions</source>
        <translation>preguntes</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="135"/>
        <source>How many &amp;players?</source>
        <translation>Quants &amp;jugadors?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="140"/>
        <source>players</source>
        <translation>jugadors</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="144"/>
        <source>Use own color theme</source>
        <translation>Utilitzar tema de colors propi</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="209"/>
        <source>Player names</source>
        <translation>Noms dels jugadors</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="225"/>
        <source>Save and &amp;reload music</source>
        <translation>Guardar i &amp;recarregar la música</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="363"/>
        <source>In this level you&apos;ll have to type the answer.</source>
        <translation>En aquest nivell hauràs d&apos;escriure la resposta.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="366"/>
        <source>It&apos;s not too strict, so you don&apos;t have to worry about caps, commas and such.</source>
        <translation>No es gaire estricte, o sigui que no t&apos;has de preocupar de les majúscules, comes, etc.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="373"/>
        <source>In this level you&apos;ll have to click the correct button, or press the 1, 2, 3 and 4 keys in your keyboard.</source>
        <translation>En aquest nivell hauràs de fer clic al botó correcte, o apretar les tecles 1, 2, 3 i 4 del teclat.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="460"/>
        <source>&amp;Save options</source>
        <translation>&amp;Guardar opcions</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="234"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="304"/>
        <source>Select where your music is located</source>
        <translation>Sel·lecciona on es troba la teva música</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="351"/>
        <source>Piece of cake</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="352"/>
        <source>Let&apos;s rock</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="353"/>
        <source>Come get some</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="354"/>
        <source>Damn I&apos;m good</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="355"/>
        <source>HARD!!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="356"/>
        <source>Hardcore! Type the answer</source>
        <translation>Molt difícil! Tecleja la resposta</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="490"/>
        <source>Start!</source>
        <translation>Començar!</translation>
    </message>
</context>
<context>
    <name>Ranking</name>
    <message>
        <location filename="../src/ranking.cpp" line="27"/>
        <source>Game ended</source>
        <translation>Joc acabat</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="33"/>
        <source>All questions done!</source>
        <translation>Has contestat totes les preguntes!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="35"/>
        <source>Thanks for playing!</source>
        <translation>Gràcies per jugar!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="36"/>
        <source>Ranking</source>
        <translation>Clasificació</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="44"/>
        <source>Player</source>
        <translation>Jugador</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="45"/>
        <source>Score</source>
        <translation>Puntuació</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="46"/>
        <source>Good</source>
        <translation>Bé</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="47"/>
        <source>Bad</source>
        <translation>Malament</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="48"/>
        <source>T.O.</source>
        <comment>Timed out abbreviation</comment>
        <translation>T.E.</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="124"/>
        <source>What?? Zero points!!
You&apos;re all sooo bad!</source>
        <comment>Plural, all players</comment>
        <translation>Què?? Zero punts!!
Sou tots mooolt dolents!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="130"/>
        <source>Zero points!
%1, you&apos;re very bad!!</source>
        <comment>%1 is the name of a player</comment>
        <translation>Zero punts!
%1, se&apos;t dóna molt malament!!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="146"/>
        <source>Draw between %1 and %2!</source>
        <comment>%1 and %2 are player names</comment>
        <translation>Empat entre %1 i %2!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="156"/>
        <source>You&apos;re very good!!</source>
        <translation>Se&apos;t dóna molt be!!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="163"/>
        <source>Congratulations, %1!!</source>
        <comment>%1 is the name of the winner</comment>
        <translation>Felicitats, %1!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="173"/>
        <source>That was amazing!!</source>
        <translation>Ha sigut increïble!!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="177"/>
        <source>That was quite good!</source>
        <translation>Ha estat força bé!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="181"/>
        <source>You can do better...</source>
        <translation>Pots fer-ho millor...</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="160"/>
        <source>You&apos;re good!</source>
        <translation>Ets bo!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="197"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
</TS>
