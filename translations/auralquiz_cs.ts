<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AnswerBox</name>
    <message>
        <location filename="../src/answerbox.cpp" line="29"/>
        <source>Type the answer here</source>
        <translation>Zde napište odpověď</translation>
    </message>
</context>
<context>
    <name>AuralWindow</name>
    <message>
        <location filename="../src/auralwindow.cpp" line="42"/>
        <source>First usage</source>
        <translation>První použití</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="264"/>
        <source>Good</source>
        <translation>Dobrý</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="273"/>
        <source>Bad</source>
        <translation>Špatný</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="282"/>
        <source>Timed out</source>
        <translation>Přestávka</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="312"/>
        <source>&amp;Start game</source>
        <translation>&amp;Spustit hru</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="321"/>
        <source>&amp;Options</source>
        <translation>&amp;Volby</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="328"/>
        <source>&amp;About...</source>
        <translation>&amp;O...</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>&amp;Ukončit</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="427"/>
        <source>Time</source>
        <translation>Čas</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="430"/>
        <source>Remaining time to answer this question</source>
        <translation>Čas zbývající na zodpovězení této otázky</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="434"/>
        <source>%v out of %m questions - %p%</source>
        <translation>%v z %m otázek - %p%</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="437"/>
        <source>How many questions you&apos;ve answered</source>
        <translation>Kolik otázek jste zodpověděl</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="439"/>
        <source>Score</source>
        <translation>Výsledek</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="446"/>
        <source>Your current score</source>
        <translation>Váš nynější výsledek</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="454"/>
        <source>&amp;End game</source>
        <translation>&amp;Ukončit hru</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="482"/>
        <source>Statistics</source>
        <translation>Statistika</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="664"/>
        <source>Sound system error</source>
        <translation>Chyba zvukového systému</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="665"/>
        <source>There seems to be a problem with your sound system.</source>
        <translation>Zdá se, že váš zvukový systém má potíže.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1122"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. Feel free to add your contact information, such as e-mail. If there was another translator before you, add your name after theirs ;)</comment>
        <translatorcomment>(note by Jan: This should read &apos;Czech translation by YourName&apos; ;)</translatorcomment>
        <translation>Překlad do češtiny: Pavel Fric.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1133"/>
        <source>Auralquiz is licensed under the GNU GPL license.</source>
        <translation>Auralquiz je licencován pod licencí GNU GPL.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1135"/>
        <source>Main screen image and some icons are based on Oxygen icons, under LGPL license.</source>
        <translation>Obrázek na hlavní obrazovce a některé ikony založen na ikonách Oxygen vydaných pod licencí LGPL.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1373"/>
        <source>Go, %1!</source>
        <comment>%1=player&apos;s name</comment>
        <translation>Jdi, %1!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1501"/>
        <source>Correct answer was:</source>
        <translation>Správná odpověď byla:</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1572"/>
        <source>End game?</source>
        <translation>Ukončit hru?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1573"/>
        <source>Are you sure you want to end this game?</source>
        <translation>Opravdu chcete tuto hru ukončit?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;Yes, end it</source>
        <translation>&amp;Ano, ukončit</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;No</source>
        <translation>&amp;Ne</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1208"/>
        <source>Player %1 of %2</source>
        <translation>Hráč %1 z %2</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="944"/>
        <source>Error playing sound</source>
        <translation>Chyba při přehrávání zvuku</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="945"/>
        <source>An error occurred while playing sound.
The error message was: %1

Maybe your Phonon-backend does not have support for the MP3 file format.</source>
        <translation>Při přehrávání zvuku se vyskytla chyba.
Chybové hlášení bylo: %1

Možná vaše jádro Phonon nemá podporu pro formát MP3.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1030"/>
        <source>Time&apos;s up!!</source>
        <translation>Čas vypršel!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1106"/>
        <source>About Auralquiz</source>
        <translation>O programu Auralquiz</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1117"/>
        <source>Auralquiz loads all your music from a specified folder and asks questions about it, playing a short piece of each music file as clue.</source>
        <translation>Auralquiz nahrává všechnu vaši hudbu ze zadané složky a ptá se na ni přehrávaje kousek z každého hudebního souboru jako nápovědu.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="43"/>
        <source>This seems to be the first time you use Auralquiz.
Before playing, your music will be analyzed.
If needed, you should click the Options button and select the folder where your Ogg, FLAC and MP3 files are stored.

This folder, and sub-folders will be scanned so Auralquiz can generate questions and answers about your music.

You need files correctly tagged in order for the game to work correctly.

The scan can take some time, and the program will not be responsive until it is complete. Please be patient.</source>
        <translation>Zdá se, že je to poprvé, kdy používáte Auralquiz.
Před začátkem hry bude vaše hudba zpracována.
V případě potřeby máte klepnout na tlačítko Volby a vybrat složku, v níž jsou uloženy vaše soubory Ogg, FLAC a MP3.

Tato složka a její podsložky budou prohledány, takže Auralquiz bude moci vytvořit otázky a odpovědi k vaší hudbě.

Aby hra běžela správně, je potřeba, aby soubory byly řádně označkovány.

Prohlídka může chvíli trvat a program nebude odpovídat, dokud nebude hotová. Mějte, prosím, trpělivost.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="668"/>
        <source>Maybe you don&apos;t have any Phonon backends installed.</source>
        <translation>Může být, že nemáte nainstalovánu podpůrnou vrstvu Phonon.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="777"/>
        <source>Starting!</source>
        <translation>Začínáme!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="778"/>
        <source>Let&apos;s go!</source>
        <translation>Pojďme na to!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="779"/>
        <source>GO!!</source>
        <translation>Pojďme!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="780"/>
        <source>Good luck!</source>
        <translation>Hodně štěstí!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="790"/>
        <source>%1 songs available</source>
        <translation>Dostupných %1 písní</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1031"/>
        <source>The answer was:</source>
        <translation>Odpověď byla:</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1129"/>
        <source>Thanks to all the testers, translators and packagers, who help make Auralquiz better!</source>
        <translation>Poděkování všem zkoušečům, překladatelům a balíčkářům, kteří Auralquiz pomohli udělat lepším!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1198"/>
        <source>Who plays this song?</source>
        <translation>Kdo hraje tuto píseň?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1202"/>
        <source>What&apos;s the title of this song?</source>
        <translation>Jaký je název této písně?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1319"/>
        <source>Not enough music files</source>
        <translation>Nedostatek hudebních souborů</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1320"/>
        <source>You don&apos;t have enough music files, or from enough different artists.
You need music from at least 5 different artists to be able to generate questions.</source>
        <translation>Nemáte dostatek hudebních souborů, nebo dost souborů od různých umělců.
Na tvoření otázek potřebujete hudbu od alespoň pěti různých umělců.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1379"/>
        <source>Next!</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1454"/>
        <source>Correct!!</source>
        <translation>Správně!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1455"/>
        <source>Yeah!</source>
        <translation>Ano!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1456"/>
        <source>That&apos;s right!</source>
        <translation>Správně!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1457"/>
        <source>Good!</source>
        <translation>Dobře!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1458"/>
        <source>Great!</source>
        <translation>Báječně!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1470"/>
        <source>%1 by %2</source>
        <comment>1=song title, 2=author name</comment>
        <translation>%1 od %2</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1475"/>
        <source>+%1 points!</source>
        <translation>+%1 bodů!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1486"/>
        <source>Wrong!</source>
        <translation>Špatně!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1487"/>
        <source>No!</source>
        <translation>Ne!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1488"/>
        <source>That&apos;s not it.</source>
        <translation>To není ono.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1489"/>
        <source>Ooops!</source>
        <translation>Jejda!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1490"/>
        <source>FAIL!</source>
        <translation>Neúspěch!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1408"/>
        <source>All done!</source>
        <translation>Vše hotovo!</translation>
    </message>
</context>
<context>
    <name>MusicAnalyzer</name>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="125"/>
        <source>Configure your music folder</source>
        <translation>Nastavte složku s hudbou</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="214"/>
        <source>Analyzing music. Please wait</source>
        <translation>Provádí se rozbor hudby. Počkejte, prosím</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="223"/>
        <source>Getting list of files under
%1</source>
        <comment>%1 is a folder</comment>
        <translation>Získává se seznam souborů v
%1</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="227"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="230"/>
        <source>Listing files...</source>
        <translation>Sestavuje se seznam souborů...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="255"/>
        <location filename="../src/musicanalyzer.cpp" line="361"/>
        <source>Please reload music info</source>
        <translation>Nahrajte, prosím, informace o hudbě</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="256"/>
        <source>Music analysis was cancelled</source>
        <translation>Rozbor hudby byl zrušen</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="269"/>
        <source>There are no valid music files in your selected music directory.</source>
        <translation>Ve vámi vybraném adresáři s hudbou nejsou žádné platné hudební soubory.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="272"/>
        <source>Please, select another directory containing Ogg, FLAC or MP3 files, and try again.</source>
        <translation>Vyberte, prosím, jiný adresář obsahující soubory Ogg, FLAC nebo MP3 a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="276"/>
        <source>There is no music!</source>
        <translation>Není tu žádná hudba!</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="280"/>
        <location filename="../src/musicanalyzer.cpp" line="385"/>
        <source>Please choose another music folder</source>
        <translation>Vyberte, prosím, jiný adresář s hudbou</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="292"/>
        <source>Analyzing %1 files
under %2</source>
        <comment>%1 is a number, %2 is a folder</comment>
        <translation>Provádí se rozbor %1 souborů
v %2</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="297"/>
        <source>Cancel analysis</source>
        <translation>Zrušit rozbor</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="300"/>
        <source>Analyzing your music...</source>
        <translation>Provádí se rozbor vaší hudby...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="386"/>
        <source>The folder you selected doesn&apos;t have enough valid music files</source>
        <translation>Vámi vybraná složka neobsahuje dost platných hudebních souborů</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="396"/>
        <source>&amp;Start game</source>
        <translation>&amp;Spustit hru</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/optionsdialog.cpp" line="31"/>
        <source>Game options</source>
        <translation>Volby pro hru</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="59"/>
        <source>Using music from:</source>
        <translation>Používá se hudba z:</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="64"/>
        <source>Select your &amp;music folder...</source>
        <translation>Vyberte složku s &amp;hudbou...</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="83"/>
        <source>&amp;Easier</source>
        <translation>&amp;Lehčí</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="98"/>
        <source>&amp;Harder</source>
        <translation>&amp;Těžší</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="126"/>
        <source>How many &amp;questions?</source>
        <translation>Kolik &amp;otázek?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="131"/>
        <source>questions</source>
        <translation>otázek</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="135"/>
        <source>How many &amp;players?</source>
        <translation>Kolik &amp;hráčů?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="140"/>
        <source>players</source>
        <translation>hráčů</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="144"/>
        <source>Use own color theme</source>
        <translation>Použít vlastní barevný motiv</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="209"/>
        <source>Player names</source>
        <translation>Jména hráčů</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="225"/>
        <source>Save and &amp;reload music</source>
        <translation>Uložit a &amp;nahrát hudbu znovu</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="234"/>
        <source>&amp;Cancel</source>
        <translation>Z&amp;rušit</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="304"/>
        <source>Select where your music is located</source>
        <translation>Vyberte, kde je vaše hudba umístěna</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="351"/>
        <source>Piece of cake</source>
        <translation>Brnkačka</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="352"/>
        <source>Let&apos;s rock</source>
        <translation>Rozjedem to</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="353"/>
        <source>Come get some</source>
        <translation>Pojďme něco sehnat</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="354"/>
        <source>Damn I&apos;m good</source>
        <translation>Sakra, jsem dobrý</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="355"/>
        <source>HARD!!</source>
        <translation>Těžké!</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="356"/>
        <source>Hardcore! Type the answer</source>
        <translation>Těžké! Napište odpověď</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="363"/>
        <source>In this level you&apos;ll have to type the answer.</source>
        <translation>Na této úrovni musíte napsat odpověď.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="366"/>
        <source>It&apos;s not too strict, so you don&apos;t have to worry about caps, commas and such.</source>
        <translation>Není to příliš přísné, takže se nemusíte obávat velikosti písmen, čárek a tak podobně.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="373"/>
        <source>In this level you&apos;ll have to click the correct button, or press the 1, 2, 3 and 4 keys in your keyboard.</source>
        <translation>Na této úrovni musíte klepnout na správné tlačítko, nebo na vaší klávesnici stisknout klávesu 1, 2, 3 nebo 4.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="460"/>
        <source>&amp;Save options</source>
        <translation>&amp;Uložit volby</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="490"/>
        <source>Start!</source>
        <translation>Začít!</translation>
    </message>
</context>
<context>
    <name>Ranking</name>
    <message>
        <location filename="../src/ranking.cpp" line="27"/>
        <source>Game ended</source>
        <translation>Hra skončena</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="33"/>
        <source>All questions done!</source>
        <translation>Všechny otázky udělány!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="35"/>
        <source>Thanks for playing!</source>
        <translation>Díky za hraní!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="36"/>
        <source>Ranking</source>
        <translation>Žebříček</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="44"/>
        <source>Player</source>
        <translation>Hráč</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="45"/>
        <source>Score</source>
        <translation>Výsledek</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="46"/>
        <source>Good</source>
        <translation>Dobrý</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="47"/>
        <source>Bad</source>
        <translation>Špatný</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="48"/>
        <source>T.O.</source>
        <comment>Timed out abbreviation</comment>
        <translation>Překr. času</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="124"/>
        <source>What?? Zero points!!
You&apos;re all sooo bad!</source>
        <comment>Plural, all players</comment>
        <translation>Cože? Žádné body?
Jste na to všichni hodně moc bledě!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="130"/>
        <source>Zero points!
%1, you&apos;re very bad!!</source>
        <comment>%1 is the name of a player</comment>
        <translation>Žádné body?
%1, jste na tom velmi špatně!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="146"/>
        <source>Draw between %1 and %2!</source>
        <comment>%1 and %2 are player names</comment>
        <translation>Remíza mezi %1 a %2!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="156"/>
        <source>You&apos;re very good!!</source>
        <translation>Jste velmi dobrý!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="163"/>
        <source>Congratulations, %1!!</source>
        <comment>%1 is the name of the winner</comment>
        <translation>Blahopřání %1!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="173"/>
        <source>That was amazing!!</source>
        <translation>To bylo úžasné!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="177"/>
        <source>That was quite good!</source>
        <translation>To bylo docela dobré!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="181"/>
        <source>You can do better...</source>
        <translation>Dokážete to udělat líp...</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="160"/>
        <source>You&apos;re good!</source>
        <translation>Jste dobrý!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="197"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
</TS>
