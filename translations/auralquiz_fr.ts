<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AnswerBox</name>
    <message>
        <location filename="../src/answerbox.cpp" line="29"/>
        <source>Type the answer here</source>
        <translation>Tapez la réponse ici</translation>
    </message>
</context>
<context>
    <name>AuralWindow</name>
    <message>
        <location filename="../src/auralwindow.cpp" line="42"/>
        <source>First usage</source>
        <translation>Première utilisation</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="43"/>
        <source>This seems to be the first time you use Auralquiz.
Before playing, your music will be analyzed.
If needed, you should click the Options button and select the folder where your Ogg, FLAC and MP3 files are stored.

This folder, and sub-folders will be scanned so Auralquiz can generate questions and answers about your music.

You need files correctly tagged in order for the game to work correctly.

The scan can take some time, and the program will not be responsive until it is complete. Please be patient.</source>
        <translation>Il semble que c&apos;est la première fois que vous utilisez Auralquiz.
Avant de jouer, votre musique sera analysée.
Si vous avez besoin, vous pouvez cliquer le bouton Options et sélectionner le dossier où se trouvent vos fichiers Ogg, FLAC et MP3.

Ce dossier, et ses sous-dossiers seront scannés pour qu&apos;Auralquiz puisse générer des questions-réponses sur votre musique.

Vous avez besoin de fichiers correctement taggés pour que le jeu fonctionne bien.

Le scan peut durer longtemps, et le programme peut ne plus répondre tant que ce n&apos;est pas fini. Soyez patient.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="312"/>
        <source>&amp;Start game</source>
        <translation>&amp;Démarrer le jeu</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="321"/>
        <source>&amp;Options</source>
        <translation>&amp;Options</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="328"/>
        <source>&amp;About...</source>
        <translation>À &amp;propos...</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="427"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="430"/>
        <source>Remaining time to answer this question</source>
        <translation>Temps restant pour répondre à cette question</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="434"/>
        <source>%v out of %m questions - %p%</source>
        <translation>%v sur %m questions - %p%</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="437"/>
        <source>How many questions you&apos;ve answered</source>
        <translation>À combien de questions vous avez répondu</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="439"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="446"/>
        <source>Your current score</source>
        <translation>Votre score actuel</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="454"/>
        <source>&amp;End game</source>
        <translation>&amp;Terminer le jeu</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="482"/>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="664"/>
        <source>Sound system error</source>
        <translation>Erreur du système de son</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="665"/>
        <source>There seems to be a problem with your sound system.</source>
        <translation>Il semble qu&apos;il y a un problème avec votre système de son.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="668"/>
        <source>Maybe you don&apos;t have any Phonon backends installed.</source>
        <translation>Peut-être que vous n&apos;avez aucun backend Phonon d&apos;installé.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="790"/>
        <source>%1 songs available</source>
        <translation>%1 chansons disponibles</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1122"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. Feel free to add your contact information, such as e-mail. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traduction française par ntome, Adrien DAUGABEL et David GEIGER.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="944"/>
        <source>Error playing sound</source>
        <translation>Erreur en jouant un son</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="945"/>
        <source>An error occurred while playing sound.
The error message was: %1

Maybe your Phonon-backend does not have support for the MP3 file format.</source>
        <translation>Une erreur est survenue en jouant un son.
Le message d&apos;erreur était : %1

Peut-être que votre backend Phonon ne supporte pas le format de fichier MP3.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1030"/>
        <source>Time&apos;s up!!</source>
        <translation>Temps écoulé !!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1106"/>
        <source>About Auralquiz</source>
        <translation>À propos d&apos;Auralquiz</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1117"/>
        <source>Auralquiz loads all your music from a specified folder and asks questions about it, playing a short piece of each music file as clue.</source>
        <translation>Auralquiz charge toute votre musique depuis un certain dossier et pose des questions dessus, en jouant un petit morceau de chaque musique comme indice.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="777"/>
        <source>Starting!</source>
        <translation>À vos marques !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="264"/>
        <source>Good</source>
        <translation>Bon</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="273"/>
        <source>Bad</source>
        <translation>Mauvais</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="282"/>
        <source>Timed out</source>
        <translation>Temps écoulé</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="778"/>
        <source>Let&apos;s go!</source>
        <translation>Prêts ?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="779"/>
        <source>GO!!</source>
        <translation>PARTEZ !!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="780"/>
        <source>Good luck!</source>
        <translation>Bonne chance !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1031"/>
        <source>The answer was:</source>
        <translation>La réponse était :</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1129"/>
        <source>Thanks to all the testers, translators and packagers, who help make Auralquiz better!</source>
        <translation>Merci à tous les testeurs, traducteurs et packageurs, qui contribuent à l&apos;amélioration d&apos;Auralquiz !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1133"/>
        <source>Auralquiz is licensed under the GNU GPL license.</source>
        <translation>Auralquiz est sous licence GNU GPL.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1135"/>
        <source>Main screen image and some icons are based on Oxygen icons, under LGPL license.</source>
        <translation>L&apos;image de l&apos;écran principal et les icônes sont basés sur les icones Oxygen, sous licence LGPL.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1198"/>
        <source>Who plays this song?</source>
        <translation>Qui joue ce morceau ?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1202"/>
        <source>What&apos;s the title of this song?</source>
        <translation>Quel est le titre de ce morceau ?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1208"/>
        <source>Player %1 of %2</source>
        <translation>Joueur %1 sur %2</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1319"/>
        <source>Not enough music files</source>
        <translation>Pas assez de fichiers musicaux</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1320"/>
        <source>You don&apos;t have enough music files, or from enough different artists.
You need music from at least 5 different artists to be able to generate questions.</source>
        <translation>Vous n&apos;avez pas assez de fichiers musicaux, ou pas assez d&apos;artistes différents.
Vous avez besoin de musique d&apos;au moins 5 artistes différents pour pouvoir générer des questions.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1373"/>
        <source>Go, %1!</source>
        <comment>%1=player&apos;s name</comment>
        <translation>Allez, %1 !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1470"/>
        <source>%1 by %2</source>
        <comment>1=song title, 2=author name</comment>
        <translation>%1 par %2</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1501"/>
        <source>Correct answer was:</source>
        <translation>La bonne réponse était :</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1572"/>
        <source>End game?</source>
        <translation>Terminer la partie ?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1573"/>
        <source>Are you sure you want to end this game?</source>
        <translation>Etes-vous sûr de vouloir terminer cette partie ?</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;Yes, end it</source>
        <translation>&amp;Oui, terminer</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1379"/>
        <source>Next!</source>
        <translation>Suivant !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1408"/>
        <source>All done!</source>
        <translation>Terminé !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1454"/>
        <source>Correct!!</source>
        <translation>Correct !!</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1455"/>
        <source>Yeah!</source>
        <translation>Yeah !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1456"/>
        <source>That&apos;s right!</source>
        <translation>C&apos;est juste !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1457"/>
        <source>Good!</source>
        <translation>Bien !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1458"/>
        <source>Great!</source>
        <translation>Génial !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1475"/>
        <source>+%1 points!</source>
        <translation>+%1 points !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1486"/>
        <source>Wrong!</source>
        <translation>Faux !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1487"/>
        <source>No!</source>
        <translation>Non !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1488"/>
        <source>That&apos;s not it.</source>
        <translation>Ce n&apos;est pas ça.</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1489"/>
        <source>Ooops!</source>
        <translation>Ouuups !</translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1490"/>
        <source>FAIL!</source>
        <translation>RATÉ !</translation>
    </message>
</context>
<context>
    <name>MusicAnalyzer</name>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="125"/>
        <source>Configure your music folder</source>
        <translation>Configurer votre dossier de musique</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="214"/>
        <source>Analyzing music. Please wait</source>
        <translation>Analyse de la musique. Veuillez patienter</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="223"/>
        <source>Getting list of files under
%1</source>
        <comment>%1 is a folder</comment>
        <translation>Obtention de la liste des fichiers dans %1</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="227"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="230"/>
        <source>Listing files...</source>
        <translation>Liste les fichiers...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="255"/>
        <location filename="../src/musicanalyzer.cpp" line="361"/>
        <source>Please reload music info</source>
        <translation>Merci de recharger les informations de la musique</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="256"/>
        <source>Music analysis was cancelled</source>
        <translation>L&apos;analyse de la musique a été annulée</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="269"/>
        <source>There are no valid music files in your selected music directory.</source>
        <translation>Il n&apos;y a pas de fichier de musique valide dans le dossier de musique sélectionné.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="272"/>
        <source>Please, select another directory containing Ogg, FLAC or MP3 files, and try again.</source>
        <translation>Veuillez sélectionner un autre dossier contenant des fichiers Ogg, FLAC ou MP3 puis réessayez.</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="276"/>
        <source>There is no music!</source>
        <translation>Il n&apos;y a pas de musique !</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="280"/>
        <location filename="../src/musicanalyzer.cpp" line="385"/>
        <source>Please choose another music folder</source>
        <translation>Veuillez sélectionner un autre dossier de musique</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="292"/>
        <source>Analyzing %1 files
under %2</source>
        <comment>%1 is a number, %2 is a folder</comment>
        <translation>Analyse %1 fichiers
dans %2</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="297"/>
        <source>Cancel analysis</source>
        <translation>Annuler l&apos;analyse</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="300"/>
        <source>Analyzing your music...</source>
        <translation>Analyse votre musique...</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="386"/>
        <source>The folder you selected doesn&apos;t have enough valid music files</source>
        <translation>Le dossier que vous avez sélectionné n&apos;a pas assez de fichiers de musique valide</translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="396"/>
        <source>&amp;Start game</source>
        <translation>&amp;Démarrer le jeu</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/optionsdialog.cpp" line="31"/>
        <source>Game options</source>
        <translation>Options du jeu</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="59"/>
        <source>Using music from:</source>
        <translation>Utilise la musique de :</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="64"/>
        <source>Select your &amp;music folder...</source>
        <translation>Sélectionnez votre dossier de &amp;musique...</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="83"/>
        <source>&amp;Easier</source>
        <translation>&amp;Plus facile</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="98"/>
        <source>&amp;Harder</source>
        <translation>&amp;Plus difficile</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="126"/>
        <source>How many &amp;questions?</source>
        <translation>Combien de &amp;questions ?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="131"/>
        <source>questions</source>
        <translation>questions</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="135"/>
        <source>How many &amp;players?</source>
        <translation>Combien de &amp;joueurs ?</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="140"/>
        <source>players</source>
        <translation>joueurs</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="144"/>
        <source>Use own color theme</source>
        <translation>Utiliser le thème de couleur spécial</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="209"/>
        <source>Player names</source>
        <translation>Noms des joueurs</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="225"/>
        <source>Save and &amp;reload music</source>
        <translation>Enregistrer et &amp;recharger la musique</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="234"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="304"/>
        <source>Select where your music is located</source>
        <translation>Sélectionnez le dossier où se trouve votre musique</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="351"/>
        <source>Piece of cake</source>
        <translatorcomment>not translated for DN3D reference</translatorcomment>
        <translation>Piece of cake</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="352"/>
        <source>Let&apos;s rock</source>
        <translatorcomment>not translated for DN3D reference</translatorcomment>
        <translation>Let&apos;s rock</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="353"/>
        <source>Come get some</source>
        <translatorcomment>not translated for DN3D reference</translatorcomment>
        <translation>Come get some</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="354"/>
        <source>Damn I&apos;m good</source>
        <translatorcomment>not translated for DN3D reference</translatorcomment>
        <translation>Damn I&apos;m good</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="355"/>
        <source>HARD!!</source>
        <translation>Difficile !!</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="356"/>
        <source>Hardcore! Type the answer</source>
        <translation>Super difficile ! Taper la réponse</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="363"/>
        <source>In this level you&apos;ll have to type the answer.</source>
        <translation>Dans ce niveau, vous devrez taper la réponse.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="366"/>
        <source>It&apos;s not too strict, so you don&apos;t have to worry about caps, commas and such.</source>
        <translation>Ce n&apos;est pas trop strict, vous n&apos;avez pas besoin de mettre les majuscules, virgules etc.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="373"/>
        <source>In this level you&apos;ll have to click the correct button, or press the 1, 2, 3 and 4 keys in your keyboard.</source>
        <translation>Dans ce niveau, vous devrez cliquer sur le bon bouton, ou appuyer sur les touches 1, 2, 3 ou 4 de votre clavier.</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="460"/>
        <source>&amp;Save options</source>
        <translation>&amp;Enregistrer les options</translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="490"/>
        <source>Start!</source>
        <translation>Démarrer !</translation>
    </message>
</context>
<context>
    <name>Ranking</name>
    <message>
        <location filename="../src/ranking.cpp" line="27"/>
        <source>Game ended</source>
        <translation>Partie terminée</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="33"/>
        <source>All questions done!</source>
        <translation>Toutes les questions sont faites!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="35"/>
        <source>Thanks for playing!</source>
        <translation>Merci d&apos;avoir joué !</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="36"/>
        <source>Ranking</source>
        <translation>Classement</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="44"/>
        <source>Player</source>
        <translation>Joueur</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="45"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="46"/>
        <source>Good</source>
        <translation>Bon</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="47"/>
        <source>Bad</source>
        <translation>Mauvais</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="48"/>
        <source>T.O.</source>
        <comment>Timed out abbreviation</comment>
        <translation>D.D.</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="124"/>
        <source>What?? Zero points!!
You&apos;re all sooo bad!</source>
        <comment>Plural, all players</comment>
        <translation>Quoi ? Aucun point !!
Mais vous êtes tous si mauvais !</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="130"/>
        <source>Zero points!
%1, you&apos;re very bad!!</source>
        <comment>%1 is the name of a player</comment>
        <translation>Aucun point !
%1, vous êtes très mauvais !!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="146"/>
        <source>Draw between %1 and %2!</source>
        <comment>%1 and %2 are player names</comment>
        <translation>Égalité entre %1 et %2 !</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="156"/>
        <source>You&apos;re very good!!</source>
        <translation>Vous êtes très bon !</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="160"/>
        <source>You&apos;re good!</source>
        <translation>Vous êtes bon !</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="163"/>
        <source>Congratulations, %1!!</source>
        <comment>%1 is the name of the winner</comment>
        <translation>Félicitations, %1 !!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="173"/>
        <source>That was amazing!!</source>
        <translation>C&apos;était formidable !!</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="177"/>
        <source>That was quite good!</source>
        <translation>C&apos;était plutôt bien !</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="181"/>
        <source>You can do better...</source>
        <translation>Tu peux mieux faire...</translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="197"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
</TS>
