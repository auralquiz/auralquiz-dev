<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AnswerBox</name>
    <message>
        <location filename="../src/answerbox.cpp" line="29"/>
        <source>Type the answer here</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AuralWindow</name>
    <message>
        <location filename="../src/auralwindow.cpp" line="42"/>
        <source>First usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="43"/>
        <source>This seems to be the first time you use Auralquiz.
Before playing, your music will be analyzed.
If needed, you should click the Options button and select the folder where your Ogg, FLAC and MP3 files are stored.

This folder, and sub-folders will be scanned so Auralquiz can generate questions and answers about your music.

You need files correctly tagged in order for the game to work correctly.

The scan can take some time, and the program will not be responsive until it is complete. Please be patient.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="264"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="273"/>
        <source>Bad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="282"/>
        <source>Timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="312"/>
        <source>&amp;Start game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="321"/>
        <source>&amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="328"/>
        <source>&amp;About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="334"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="427"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="430"/>
        <source>Remaining time to answer this question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="434"/>
        <source>%v out of %m questions - %p%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="437"/>
        <source>How many questions you&apos;ve answered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="439"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="446"/>
        <source>Your current score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="454"/>
        <source>&amp;End game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="482"/>
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="664"/>
        <source>Sound system error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="665"/>
        <source>There seems to be a problem with your sound system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="668"/>
        <source>Maybe you don&apos;t have any Phonon backends installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="777"/>
        <source>Starting!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="778"/>
        <source>Let&apos;s go!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="779"/>
        <source>GO!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="780"/>
        <source>Good luck!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="790"/>
        <source>%1 songs available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="944"/>
        <source>Error playing sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="945"/>
        <source>An error occurred while playing sound.
The error message was: %1

Maybe your Phonon-backend does not have support for the MP3 file format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1030"/>
        <source>Time&apos;s up!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1031"/>
        <source>The answer was:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1106"/>
        <source>About Auralquiz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1117"/>
        <source>Auralquiz loads all your music from a specified folder and asks questions about it, playing a short piece of each music file as clue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1122"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. Feel free to add your contact information, such as e-mail. If there was another translator before you, add your name after theirs ;)</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1129"/>
        <source>Thanks to all the testers, translators and packagers, who help make Auralquiz better!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1133"/>
        <source>Auralquiz is licensed under the GNU GPL license.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1135"/>
        <source>Main screen image and some icons are based on Oxygen icons, under LGPL license.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1198"/>
        <source>Who plays this song?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1202"/>
        <source>What&apos;s the title of this song?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1208"/>
        <source>Player %1 of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1319"/>
        <source>Not enough music files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1320"/>
        <source>You don&apos;t have enough music files, or from enough different artists.
You need music from at least 5 different artists to be able to generate questions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1373"/>
        <source>Go, %1!</source>
        <comment>%1=player&apos;s name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1379"/>
        <source>Next!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1408"/>
        <source>All done!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1454"/>
        <source>Correct!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1455"/>
        <source>Yeah!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1456"/>
        <source>That&apos;s right!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1457"/>
        <source>Good!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1458"/>
        <source>Great!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1470"/>
        <source>%1 by %2</source>
        <comment>1=song title, 2=author name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1475"/>
        <source>+%1 points!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1486"/>
        <source>Wrong!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1487"/>
        <source>No!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1488"/>
        <source>That&apos;s not it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1489"/>
        <source>Ooops!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1490"/>
        <source>FAIL!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1501"/>
        <source>Correct answer was:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1572"/>
        <source>End game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1573"/>
        <source>Are you sure you want to end this game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;Yes, end it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/auralwindow.cpp" line="1575"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MusicAnalyzer</name>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="125"/>
        <source>Configure your music folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="214"/>
        <source>Analyzing music. Please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="223"/>
        <source>Getting list of files under
%1</source>
        <comment>%1 is a folder</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="227"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="230"/>
        <source>Listing files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="255"/>
        <location filename="../src/musicanalyzer.cpp" line="361"/>
        <source>Please reload music info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="256"/>
        <source>Music analysis was cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="269"/>
        <source>There are no valid music files in your selected music directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="272"/>
        <source>Please, select another directory containing Ogg, FLAC or MP3 files, and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="276"/>
        <source>There is no music!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="280"/>
        <location filename="../src/musicanalyzer.cpp" line="385"/>
        <source>Please choose another music folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="292"/>
        <source>Analyzing %1 files
under %2</source>
        <comment>%1 is a number, %2 is a folder</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="297"/>
        <source>Cancel analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="300"/>
        <source>Analyzing your music...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="386"/>
        <source>The folder you selected doesn&apos;t have enough valid music files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/musicanalyzer.cpp" line="396"/>
        <source>&amp;Start game</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../src/optionsdialog.cpp" line="31"/>
        <source>Game options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="59"/>
        <source>Using music from:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="64"/>
        <source>Select your &amp;music folder...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="83"/>
        <source>&amp;Easier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="98"/>
        <source>&amp;Harder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="126"/>
        <source>How many &amp;questions?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="131"/>
        <source>questions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="135"/>
        <source>How many &amp;players?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="140"/>
        <source>players</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="144"/>
        <source>Use own color theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="209"/>
        <source>Player names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="225"/>
        <source>Save and &amp;reload music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="234"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="304"/>
        <source>Select where your music is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="351"/>
        <source>Piece of cake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="352"/>
        <source>Let&apos;s rock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="353"/>
        <source>Come get some</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="354"/>
        <source>Damn I&apos;m good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="355"/>
        <source>HARD!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="356"/>
        <source>Hardcore! Type the answer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="363"/>
        <source>In this level you&apos;ll have to type the answer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="366"/>
        <source>It&apos;s not too strict, so you don&apos;t have to worry about caps, commas and such.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="373"/>
        <source>In this level you&apos;ll have to click the correct button, or press the 1, 2, 3 and 4 keys in your keyboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="460"/>
        <source>&amp;Save options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/optionsdialog.cpp" line="490"/>
        <source>Start!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ranking</name>
    <message>
        <location filename="../src/ranking.cpp" line="27"/>
        <source>Game ended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="33"/>
        <source>All questions done!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="35"/>
        <source>Thanks for playing!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="36"/>
        <source>Ranking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="44"/>
        <source>Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="45"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="46"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="47"/>
        <source>Bad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="48"/>
        <source>T.O.</source>
        <comment>Timed out abbreviation</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="124"/>
        <source>What?? Zero points!!
You&apos;re all sooo bad!</source>
        <comment>Plural, all players</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="130"/>
        <source>Zero points!
%1, you&apos;re very bad!!</source>
        <comment>%1 is the name of a player</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="146"/>
        <source>Draw between %1 and %2!</source>
        <comment>%1 and %2 are player names</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="156"/>
        <source>You&apos;re very good!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="160"/>
        <source>You&apos;re good!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="163"/>
        <source>Congratulations, %1!!</source>
        <comment>%1 is the name of the winner</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="173"/>
        <source>That was amazing!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="177"/>
        <source>That was quite good!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="181"/>
        <source>You can do better...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ranking.cpp" line="197"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
